# Introduction

This Vagrant environment is prepared for Flask apps. 

Vagrant and Virtual Box should be set up on your system.  


## Installation and Usage

1 - Use Git Bash to download the code repo. 

    - git clone https://gozdeyavuz@bitbucket.org/gozdeyavuz/vagrant_flask_challenge.git -- To track commit history please check this repo.
	
		or 
		
	- git clone https://github.com/hanifegozdeyavuz/vagrant_flask_challenge.git

2 - Open Command Prompt / Terminal.

3 - Go to project directory. 

     - Example : cd ./vagrant_flask_challenge

4 - Use below Vagrant commands; 

     - vagrant up

5 - After terminal is done , you can see virtual environment on Virtual Box with state "running". 

6 - Go back to terminal, use below command to connect your vagrant environment;

     - vagrant ssh

7 - Once it is connected, go to project directory.

     - cd /vagrant

8 - For this vagrant environment Gunicorn Server has been used. Use below command, to run gunicorn and to see the application on the host machine's browser;

     - gunicorn --bind 172.28.138.35:5000 wsgi:app

9 - After above command is executed go to your browser and paste below link;

     - http://172.28.138.35:5000

10 - Now you are looking at the Flask application on your browser.

11 - You can find "app.py" file which is main flask application. On your local, you can edit "app.py", stop running gunicorn and run it again with below command;

     - gunicorn --bind 172.28.138.35:5000 wsgi:app

12 - Go to your browser again and refresh the page or paste below address again;

     - http://172.28.138.35:5000

13 - Now you can see your edit on the browser. 